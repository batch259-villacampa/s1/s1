<?php

// to run php : $ php -S localhost:8000 in cmd

// echo "Hello";

// Comments (Single line)
/*
	(Multi-line)
	There are two types of comments:
	- Single-line comment - ctrl + /
	- Multi-line comment - ctrl + shift + /
*/

// Variables 
	// - are used to contain data.
	// - are defined using the dollar symbol ($) notation before the name of the variable
	// (" ") of (' ')

	$name = 'John Smith';
	$email = 'johnsmith@gmail.com';

// Constants
	// Constants used to hold taht are meant to be read-only.
	// Constants are defined using the define() function.
	define('PI', 3.1416);
	define('student', 'Joseph');

	$student = 'Joseph';
	$PI = 3.1416;

// Echoing Values


// Data Types

	// 1. Strings

	$state = 'New York';
	$country = 'United States of America';
	$address = $state.', '.$country; //Concatenation via . sign
	$address = "$state, $country"; // Concatenation via double quotes

	// 2. Integers / Whole Numbers

	$age = 18;
	$headCount = 16;
	

	// 3. Float / Decimal Numbers

	$grade = 98.2;
	$distanceInKilometers = 1234.89;

	// 4. Boolean

	$hasTravelledAbroad = false;
	$haveSymptoms = true;

	// 5. Null
	$spouse = null;
	$middleName = null;

	// 6. Arrays 
	$grades = array(98.7, 92.1, 90.2, 94.6);
	$animals = ["Dog", "Cat", "Chicken"];

	//  7. Objects
	$gradesObj = (object)[
		'firstGrading' => 98.7,
		'secondGrading' => 92.1,
		'thirdGrading' => 90.2,
		'fourthGrading' => 94.6
	];

	$personObj = (object)[
		'fullName' => 'John Smith',
		'isMarried' => false,
		'age' => 18,
		'address' => (object)[
			'state' => 'New York',
			'country' => 'United States of America'
		]

	];

//Operators

	// Assignment Operators
	$x = 250;
	$y = 120;

	$isLegalAge = true;
	$isRegistered = false;


// Functions

	function getFullName($firstName, $middleInitial, $lastName){
		return "$lastName, $firstName $middleInitial.";
	}

// Selection Control Structures
	// If-ElseIf-Else Statement

	function determineTyphoonIntensity($windSpeed){
		if($windSpeed < 30){
			return 'Not a typhoon yet.';
		}else if($windSpeed <= 61){
			return 'Tropical depression detected';
		}else if($windSpeed >= 62 && $windSpeed <= 88){
			return 'Tropical storm detected';
		}else if($windSpeed >= 89 && $windSpeed <= 117){
			return 'Severe tropical storm detected';
		}else{
			return 'Typhoon detected';
		}

	}

// Conditional (Ternary) Operator

	function isUnderAge($age){
		return ($age < 18) ? true : false;
	}

// Switch Statement

	function determineComputerUser($computerNumber){
		switch($computerNumber){
			case 1:
				return 'Linus Torvalds';
				break;
			case 2:
				return 'Steve Jobs';
				break;
			case 3:
				return 'Sid Meier';
				break;
			case 4:
				return 'Onel de Guzman';
				break;
			case 5:
				return 'Christian Salvador';
				break;
			default:
				return $computerNumber . 'is out of bounds.';
				break;

		}
	}

// Try-Catch-Finally Statement
	function greeting($str){
		try{
			if(gettype($str) === "string"){
				echo $str;
			}else{
				throw new Exception("Oops!");
			}
		}

		catch(Exception $e){
			echo $e -> getMessage();
		}

		finally{
			echo ", I did it again!";
		}

	}